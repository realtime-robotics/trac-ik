trac-ik (1.4.5-12) unstable; urgency=medium

  [ Peter Englert ]
  * [91826b7cb0] access to intermediate samples
  * [4519db3c73] added set functions for max_iterations, max_step_size
  * [57a50fe6e8] Rediff patches.
    Add 0035-access-to-intermediate-IK-samples.patch: <REASON>
    Drop 0035-access-to-intermediate-samples.patch: <REASON>
    Drop 0036-added-set-functions-for-max_iterations-max_step_size.patch: <REASON>

 -- Neil Roza <neil@rtr.ai>  Thu, 09 Feb 2023 19:21:07 +0000

trac-ik (1.4.5-11) unstable; urgency=medium

  [ Peter Englert ]
  * [4e536914eb] added wdls_ik_solver

 -- Neil Roza <neil@rtr.ai>  Mon, 30 Jan 2023 17:14:10 +0000

trac-ik (1.4.5-10) unstable; urgency=medium

  * [c2770eb3d9] add debian/watch

 -- Neil Roza <neil@rtr.ai>  Thu, 26 Jan 2023 19:55:46 +0000

trac-ik (1.4.5-9) unstable; urgency=medium

  * [614cc52dfc] edit debian/gbp.conf: upstream-branch=upstream/latest

 -- Neil Roza <neil@rtr.ai>  Mon, 23 Jan 2023 15:08:55 +0000

trac-ik (1.4.5-8) unstable; urgency=medium

  * [157a14a235] add debian/gbp.conf

 -- Neil Roza <neil@rtr.ai>  Mon, 23 Jan 2023 14:52:46 +0000

trac-ik (1.4.5-7) unstable; urgency=medium

  * wrap-and-sort --wrap-always --short-indent --sort-binary-packages --trailing-comma

 -- Neil Roza <neil@rtr.ai>  Fri, 28 Jan 2022 21:18:40 +0000

trac-ik (1.4.5-6) unstable; urgency=medium

  * no more boost log

 -- Neil Roza <neil@rtr.ai>  Thu, 27 Jan 2022 17:00:15 +0000

trac-ik (1.4.5-5) unstable; urgency=medium

  * bye ROS
  * packaging: bye ROS
  * edit debian/rules: rip out even moar ROS

 -- Neil Roza <neil@rtr.ai>  Tue, 25 Jan 2022 19:07:46 +0000

trac-ik (1.4.5-4) unstable; urgency=medium

  * edit debian/control: Build-Depends: ros-foxy-kdl-parser -> libkdl-parser-dev

 -- Neil Roza <neil@rtr.ai>  Tue, 25 Jan 2022 17:07:06 +0000

trac-ik (1.4.5-3) unstable; urgency=medium

  * edit Makefile.am: add _LDADD
  * edit Makefile.am: LDADD -> LIBADD
  * edit debian/rules: add DEB_LDFLAGS_MAINT_APPEND
  * edit configure.ac: PKG_CHECK_MODULES nlopt_cxx
  * edit debian/rules: override_dh_shlibdeps
  * edit debian/rules: try dh_shlibdeps -- --admindir
  * edit debian/rules: dh_shlibdeps -l$(ROS_ROOT)/lib
  * edit debian/rules: remove noisy diagnostics

 -- Neil Roza <neil@rtr.ai>  Wed, 19 Jan 2022 23:32:11 +0000

trac-ik (1.4.5-2) unstable; urgency=medium

  * update debian/control
  * update debian/control
  * rename debian/*.install
  * update debian/libtrac-ik-dev.install
  * update debian/libtrac-ik-dev.install
  * update debian/libtrac-ik-dev.install
  * update debian/rules
  * add debian/patches
  * update debian/patches
  * fix #include directives
  * remove namespace rtr
  * edit Makefile.am: add utils header and source
  * move headers from .h to .hpp
  * fix include directive
  * fix moar include directive
  * edit debian/contol: remove cmake deps
  * Rediff patches.
    Drop 0022-add-trac_ik-src-include-as-committed-by-Arne.patch: <REASON>
    Drop 0027-fix-include-directive.patch: <REASON>
    Drop 0028-fix-moar-include-directive.patch: <REASON>
    Drop 0026-move-headers-from-.h-to-.hpp.patch: <REASON>
    Drop 0025-edit-Makefile.am-add-utils-header-and-source.patch: <REASON>
    Drop 0024-remove-namespace-rtr.patch: <REASON>
    Drop 0023-fix-include-directives.patch: <REASON>
  * Rediff patches.
    Add 0022-adding-det-trac-ik-as-committed-by-Arne.patch: <REASON>
    Add 0023-edit-Makefile.am-fix-source-names.patch: <REASON>
    Add 0024-remove-all-RTRisms-and-clean-it-up.patch: <REASON>
  * add .clang-format
  * Rediff patches.
    Add 0022-add-RTR-trac-ik.patch: <REASON>
    Add 0023-remove-all-RTRisms-and-clean-it-up.patch: <REASON>
    Add 0024-edit-Makefile.am-fix-source-names.patch: <REASON>
    Drop 0023-edit-Makefile.am-fix-source-names.patch: <REASON>
    Drop 0025-add-.clang-format.patch: <REASON>
    Drop 0022-adding-det-trac-ik-as-committed-by-Arne.patch: <REASON>
    Drop 0024-remove-all-RTRisms-and-clean-it-up.patch: <REASON>
  * edit Makefile.am: fix EXTRA_DIST
  * Rediff patches.
    Add 0026-edit-trac_ik_lib-src-utils.cpp-assert.h-cassert.patch: <REASON>
    Add 0027-edit-trac_ik_lib-include-trac_ik-utils.hpp-stdlib.h-.patch: <REASON>
    Add 0028-edit-configure.ac-remove-std-header-detection.patch: <REASON>

 -- Neil Roza <neil@rtr.ai>  Mon, 17 Jan 2022 17:05:38 +0000

trac-ik (1.4.5-1) unstable; urgency=medium

  * Initial release (Closes: #nnnn)  <nnnn is the bug number of your ITP>

 -- Neil Roza <neil@rtr.ai>  Thu, 13 Jan 2022 18:27:55 +0000
